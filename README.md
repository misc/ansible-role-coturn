Ansible module used to manage coturn

# Usage

```
$ cat deploy_coturn.yml
- hosts: coturn
  roles:
  - role: coturn
    realm: turn.example.org
```

# Password and static key

If the variable `static_key` is present, it will be used as a key.

Otherwise, a key will be generated and stored on the controller.

# SSL Certificates

If the variable `cert_path` and `private_key_path` are defined, coturn will
use them for TLS. 
